<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Student Registration Form</title>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
   
  
    <script>
  $(document).ready(function() {
    $("#datepicker").datepicker({ dateFormat: 'dd/mm/yy' }).val();
    var date = new Date(); 
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    if (month < 12) month = "" + month;
    if (day < 30) day = "" + day;

    var today = day + "/" + month + "/" + year;       
    document.getElementById("datepicker").value = today;  
  });
  
  </script>
           <script src="~/Scripts/jquery-1.10.2.min.js"></script>  
    <script type="text/javascript">  
      
      $(document).ready(function () {  
    	 
            
            $("#nationality").change(function () {  
                $("#state").values();  
                $.ajax({  
                    type: 'POST',  
                    url: '@Url.Action("getstates")',  
                   
                     dataType: 'String',  
      
                    data: { id: $("#nationality").val() },  
      
      
                    success: function (state) { 
                    	
                    	
                        $.each(state, function (i, state) {  
                            $("#state").append('<option value="' + state.Value + '">' +  
                            		state.Text + '</option>');   
                            $('select:selected').trigger('change');
                            
                        });
                         
                    },  
                    
                    error: function (ex) {  
                        alert('Failed to retrieve nationality state.' + ex);  
                    }  
                    
                });  
                return false;  
            }) 
            document.getElementById("nationality").value = state;
        }); 
      
    </script>   
    
    
  
	

<style>

	.error {
		color: #ff0000;
	}
</style>

</head>

<body>

	<h2>Registration Form</h2>
 
	<form:form method="POST" modelAttribute="student">
		<form:input type="hidden" path="id" id="id"/>
		<table>
			<tr>
				<td><label for="name">Name: </label> </td>
				<td><form:input path="name" id="name" required="autofocus" pattern="[A-Za-z]{1,100}" /></td>
				<td><form:errors path="name" cssClass="error"/></td>
		    </tr>
	    
			 <tr>
 <td><label for="enteringDate">Entering Date: </label> </td>
 <td><form:input path="enteringDate" id="datepicker"/></td>
 <td><form:errors path="enteringDate" cssClass="error"/></td>
     </tr>
			 <tr>
                <td><label for="nationality">country: </label> </td>
               <td><form:select path="nationality">
	                      <form:option value=" " label=" " />
	                      <form:options items="${nationality}" />
	                       </form:select>
	            </td>                    
                <td><form:errors path="nationality" cssClass="error" /></td>
            </tr>
            	   
 <tr>
           <td><label  for="state">state:  </label> </td>
             <td><form:select path="state" >
            
            
  <!--  $('select').click(
   function(){
   $(this).trigger('change');
  }); -->
  
                   <form:option value=" " label=" " />
                  <form:options items="${state}" />
                      </form:select>
  </td>
	                                
                <td><form:errors path="state" cssClass="error" /></td>
     </tr>
			<tr>
				<td><label for="code">CODE: </label> </td>
				<td><form:input path="code" id="code"/></td>
				<td><form:errors path="code" cssClass="error"/></td>
		    </tr>
			    
<tr>
 <td><label for="phoneNumber" >Phone_number:  </label> </td>
 <td><form:input path="phoneNumber" id="phoneNumber" min="10" max="10"/></td>
 <td><form:errors path="phoneNumber" cssClass="error"/></td>
     </tr>
		     
	
			<tr>
				<td colspan="3">
					<c:choose>
						<c:when test="${edit}">
							<input type="submit" value="Update"/>
						</c:when>
						<c:otherwise>
							<input type="submit" value="Register"/>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</table>
	</form:form>
	<br/>
	<br/>
	Go back to <a href="<c:url value='/list' />">List of All Students</a>
</body>
</html>