package com.javabycode.springmvc.configuration;

import javax.sql.DataSource;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.javabycode.springmvc.dao.StudentDao;
import com.javabycode.springmvc.dao.StudentDaoImpl;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.javabycode.springmvc")
public class MyWebConfig {
	
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");

		return viewResolver;
	}
	
	@Bean
	public MessageSource messageSource() {
	    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
	    messageSource.setBasename("messages");
	    return messageSource;
	}
	/* @Bean
	    public StudentDao getContactDao() {
	        return new StudentDaoImpl(getDataSource());
	    }

	private DataSource getDataSource() {
		// TODO Auto-generated method stub
		return null;
	}*/
	
}

