package com.javabycode.springmvc.dao;


import java.util.List;

import com.javabycode.springmvc.model.State;

public interface StateDao {
	List<String> findAllStates(String state);
}
