package com.javabycode.springmvc.dao;



import java.util.List;

import com.javabycode.springmvc.model.Nationality;


public interface NationalityDao {
	List<Nationality> findAllCountries();
	

}
