package com.javabycode.springmvc.dao;



import java.util.List;

import org.hibernate.Criteria;

import org.springframework.stereotype.Repository;

import com.javabycode.springmvc.model.Nationality;
import com.javabycode.springmvc.model.State;


@Repository("StateDao")
public class StateDaoImpl extends AbstractDao<Integer, State> implements StateDao{
	@Override                      
	   public List<String> findAllStates(String state) {
			Criteria criteria = createEntityCriteria();
			return (List<String>) criteria.list();  
	}

	
}
