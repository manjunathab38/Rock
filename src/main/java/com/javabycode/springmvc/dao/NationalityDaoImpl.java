package com.javabycode.springmvc.dao;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.javabycode.springmvc.model.Nationality;

@Repository("NationalityDao")
public class NationalityDaoImpl extends AbstractDao<Integer, Nationality> implements NationalityDao {

	@Override
	public List<Nationality> findAllCountries() {
		Criteria criteria = createEntityCriteria();
		return (List<Nationality>) criteria.list();                                               
	}

	/*@Override                      
   public List<State> findAllStates() {
		Criteria criteria = createEntityCriteria();
		return (List<State>) criteria.list();  
		}*/
		//select state from country n where n.nationality ='India'
		
		/*Query query = getSession().createSQLQuery("select state from country where nationality=:state");
		query.setParameter("state", state);
		//List<String> list = new ArrayList<String>(); 
		 //list.add(new String { Text = "--Select Country--", Value = "0" });
      
		query.setString("country", country);
		query.executeUpdate(); 
		 
		 
		return query.list();
		
	*/
	
}
