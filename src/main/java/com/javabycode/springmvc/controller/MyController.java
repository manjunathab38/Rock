package com.javabycode.springmvc.controller;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest.*;
import javax.validation.Valid;
import javax.validation.Valid.*;
import javax.validation.Validator;

import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.javabycode.springmvc.dao.StudentDao;
import com.javabycode.springmvc.model.Nationality;
import com.javabycode.springmvc.model.State;
import com.javabycode.springmvc.model.Student;
import com.javabycode.springmvc.service.StudentService;

@Controller

@RequestMapping("/")

public class MyController {

	@Autowired
	StudentService service;
	
	@Autowired
	MessageSource messageSource;
	
	/*
	 * List all existing Students.
	 */
	@RequestMapping(value = { "/", "/list" }, method = RequestMethod.GET)
	public String listStudents(ModelMap model) {

		List<Student> students = service.findAllStudents();
		model.addAttribute("students", students);
		return "allstudents";
	}
	
	@ModelAttribute("nationality")
	   public List<Nationality> getNationality()
    {
		return service.findAllCountries();
		
        
       
     /*    ArrayList<String> n = new ArrayList<String>();
        n.add("India");
        n.add("Japan");
        n.add("USA");
        n.add(" New york");
        n.add(" South Africa");
        n.add("Australia");
        n.add("Lodan");
       return n;*/
    }
	@ModelAttribute("state")
	   public List<String> getState(String state)
 {
		
		return service.findAllStates(state);
 }

	 @RequestMapping(value = "/new/lookupStateWithinNationality", method = RequestMethod.GET)
	    @ResponseBody
	    public List<String> lookupStateWithinNationality(
	        @RequestParam(value="searchId") String searchId) {
		 
		System.out.println("========= ===================== ================== ================== =================");
		 List<String> state = service.findAllStates(searchId);
		 for (int i = 0; i < state.size(); i++) {
			System.out.println("============= "+state.get(i));
		}
	        return service.findAllStates(searchId);
	    }
	 /*Query query = getSession().createSQLQuery("select state from country where nationality=:state");
		query.setParameter("state", state);
		//List<String> list = new ArrayList<String>(); 
		 //list.add(new String { Text = "--Select Country--", Value = "0" });
   
		query.setString("country", country);
		query.executeUpdate(); 
		 
		 
		return query.list();*/
		
	/*@ModelAttribute("state")
	   public List getState(String country)
 {
		List<String> statesList = service.findAllStates(country);
		for (String string : statesList) {
			System.out.println("states : "+string);
		}
		return service.findAllStates(country);
 }*/

	/*
	 * Add a new Student.
	 */
	@RequestMapping(value = { "/new" }, method = RequestMethod.GET)
	public String newStudent(ModelMap model) {
		Student student = new Student();
		
		model.addAttribute("student", student);
		model.addAttribute("edit", false);
		
		
		return "registration";
	}
		
	/*
	 * Handling POST request for validating the user input and saving Student in database.
	 */
	@RequestMapping(value = { "/new" }, method = RequestMethod.POST)
	public String saveStudent(@Valid Student student, BindingResult result,
			ModelMap model) {

		if (result.hasErrors()) {
			return "registration";
		}
		
		if(!service.isStudentCodeUnique(student.getId(), student.getCode())){
			FieldError codeError =new FieldError("Student","code",messageSource.getMessage("non.unique.code", new String[]{student.getCode()}, Locale.getDefault()));
		    result.addError(codeError);
			return "registration";
		}
		
		service.saveStudent(student);

		model.addAttribute("success", "Student " + student.getName() + " registered successfully");
		return "success";
	}


	/*
	 * Provide the existing Student for updating.
	 */
	@RequestMapping(value = { "/edit-{code}-student" }, method = RequestMethod.GET)
	public String editStudent(@PathVariable String code, ModelMap model) {
		Student student = service.findStudentByCode(code);
		model.addAttribute("student", student);
		model.addAttribute("edit", true);
		return "registration";
	}
	
	/*
	 * Handling POST request for validating the user input and updating Student in database.
	 */
	@RequestMapping(value = { "/edit-{code}-student" }, method = RequestMethod.POST)
	public String updateStudent(@Valid Student student, BindingResult result,
			ModelMap model, @PathVariable String code) {

		if (result.hasErrors()) {
			return "registration";
		}

		if(!service.isStudentCodeUnique(student.getId(), student.getCode())){
			FieldError codeError =new FieldError("Student","code",messageSource.getMessage("non.unique.code", new String[]{student.getCode()}, Locale.getDefault()));
		    result.addError(codeError);
			return "registration";
		}

		service.updateStudent(student);

		model.addAttribute("success", "Student " + student.getName()	+ " updated successfully");
		return "success";
	}

	
	/*
	 * Delete an Student by it's CODE value.
	 */
	@RequestMapping(value = { "/delete-{code}-student" }, method = RequestMethod.GET)
	public String deleteStudent(@PathVariable String code) {
		service.deleteStudentByCode(code);
		return "redirect:/list";
	}
	
}